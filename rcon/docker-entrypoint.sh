#!/bin/bash

rcon() {
    python3 /rcon.py $@
}

retain() {
    DIR=$1
    RETENTION=$2
    find "$DIR" -not -newermt "$RETENTION" -type f -delete
}

archive() {
    DIR=$1
    FILENAME=$2
    tar -czf "$DIR/$FILENAME.tar.gz" --exclude session.lock /world
}

backup() {
    DIR=$1
    RETENTION=$2
    FILENAME=$3
    mkdir -p "$DIR"
    retain $DIR $RETENTION
    rcon save-off
    rcon save-all
    archive $DIR $FILENAME
    rcon save-on
}

restore() {
    DIR=$1
    FILENAME=$2
    rcon stop
    tar -xzf "$DIR/$FILENAME.tar.gz"
}

case $1 in
    "backup")
        if [ "$2" = "monthly" ]; then
            backup /backups/monthly "${3:-1} month ago" "$(date +%B)"
        elif [ "$2" = "weekly" ]; then
            backup /backups/weekly "${3:-1} week ago" "W$(date +%V)"
        elif [ "$2" = "daily" ]; then
            backup /backups/daily "${3:-1} days ago" "$(date +%A)"
        elif [ "$2" = "hourly" ]; then
            backup /backups/hourly "${3:-1} minutes ago" "$(date +%H)H"
        fi
        ;;
    "archive")
        archive /backups "${2:-world}"
        ;;
    "restore")
        restore /backups "${2:-world}"
        ;;
    "help"|"give"|"weather"|"gamemode"|"summon"|"tp"|"time set"|"xp"|"locatebiome"|"msg"|"ops"|"perf"|"save-all"|"stop"|"restart"|"worldborder"|"whitelist"|"gamerule"|"ban"|"ban-ip"|"pardon"|"banlist"|"clear"|"datapack"|"op"|"deop"|"difficulty"|"seed"|"save-all"|"save-off"|"save-on"|"save")
        rcon "$@"
        ;;
    *)
        exec "$@"
        ;;
esac
