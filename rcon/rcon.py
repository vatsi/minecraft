import os
import sys
import mctools
import socket

MC_RCON_PASSWORD = os.environ.get('MC_RCON_PASSWORD', '123456')

try:
    rcon = mctools.RCONClient('server')
except:
    print('could not connect rcon')
    exit(1)

try:
    if rcon.login(MC_RCON_PASSWORD):
        print(rcon.command(' '.join(sys.argv[1:])))
        exit(0)
    else:
        print('login failed.')
except:
    print('could not connect rcon')
    
exit(1)
