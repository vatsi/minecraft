#!/bin/bash
set -e

if [ "$1" = 'run' ]; then
    sed -i "
        s/^level-seed=.*$/level-seed=$MC_LEVEL_SEED/g
        s/^enable-rcon=.*$/enable-rcon=true/g
        s/^gamemode=.*$/gamemode=$MC_GAMEMODE/g
        s/^motd=.*$/motd=$MC_MOTD/g
        s/^difficulty=.*$/difficulty=$MC_DIFFICULTY/g
        s/^max-players=.*$/max-players=$MC_MAX_PLAYERS/g
        s/^online-mode=.*$/online-mode=$MC_ONLINE_MODE/g
        s/^view-distance=.*$/view-distance=$MC_VIEW_DISTANCE/g
        s/^white-list=.*$/white-list=$MC_WHITELIST/g
        s/^enforce-whitelist=.*$/enforce-whitelist=$MC_WHITELIST/g
        s/^rcon.password=.*$/rcon.password=$MC_RCON_PASSWORD/g
        s/^enable-command-block=.*$/enable-command-block=$MC_ENABLE_COMMAND_BLOCK/g
    " /minecraft/server.properties
    exec java -Xmx$MC_MEMORY -Xms$MC_MEMORY -jar /minecraft/server.jar nogui
fi

exec "$@"
