#!/bin/bash
set -e

PROTOCOL_VERSION=$(echo -en "\x10\x00\xf6\x05\x09\x6c\x6f\x63\x61\x6c\x68\x6f\x73\x74\x63\xdd\x01\x01\x00" | nc -q5 localhost 25565 | cut -c4- | jq '.version.protocol')

if (( $PROTOCOL_VERSION > 0 )); then
    exit 0;
fi

exit 1;
